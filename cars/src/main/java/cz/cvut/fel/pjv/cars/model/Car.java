package cz.cvut.fel.pjv.cars.model;

import java.util.UUID;

public class Car {
    private String manufacturer;
    private String modelName;
    private int year;
    private UUID vinCode;
    private static int carCounter;

    public Car(String manufacturer, String modelName, int year) {
        this.manufacturer = manufacturer;
        this.modelName = modelName;
        this.year = year;
        this.vinCode = UUID.randomUUID();
        carCounter++;
    }

    public String toString() {
        return String.format("%s %s year %d VIN: %s", manufacturer, modelName, year, vinCode);
    }

    public static int getNumberOfExistingCars(){
        return carCounter;
    }

    public boolean equals(Car car) {
        return vinCode.equals(car.vinCode);
    }
}
