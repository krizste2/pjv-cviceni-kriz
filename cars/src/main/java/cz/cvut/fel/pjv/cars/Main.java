package cz.cvut.fel.pjv.cars;

import cz.cvut.fel.pjv.cars.model.Car;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("Volkswagen", "Polo", 2010);
        System.out.println(car1);
        System.out.println("Počet aut: " + Car.getNumberOfExistingCars());
        Car car2 = new Car("Chevrolet", "Corvette", 1980);
        System.out.println(car2);
        System.out.println("Počet aut: " + Car.getNumberOfExistingCars());
        System.out.println(car1.equals(car2));
    }
}